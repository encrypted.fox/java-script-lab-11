import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('filesInput') filesInput: ElementRef;
  @ViewChild('photo') photo: ElementRef;
  files = [];

  onOpenFilesInput() {
    this.filesInput.nativeElement.click();
  }

  onFilesChange(file: FileList) {
    this.files = this.files.concat(file);
  }

  onFilesChangeInput(event) {
    for (let i = 0; i < event.srcElement.files.length; i++) {
      this.files.push(event.srcElement.files[i]);
    }
  }
  deleteFromList(file) {
    let markForDelete = this.files.indexOf(file);
    this.files.splice(markForDelete, 1);
  }
}
